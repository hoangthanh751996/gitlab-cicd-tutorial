"use strict";

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const morgan = require("morgan");

// load dependencies

// boostrap app
const app = express();
const server = require("http").Server(app);

app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.get("/", (req, res) => res.json({"This is": "Hello-world"}));

server.listen(30000, function (err) {
    if (err) throw err;

    console.log("Server is listening on port " + 30000);
});
